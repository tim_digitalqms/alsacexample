package tdb;

import java.util.Date;


public class Dog{
	String name;
	int age = 0;
	Date birthday;
	
	
	
	public Dog( String s ){
		super();
		setName( s );
	}
	
	
	public String toString(){
		return name;
	}
	
	
	public void setName( String s ){
		name = s;
	}
	
	
	public String getName(){
		return name;
	}
	
}