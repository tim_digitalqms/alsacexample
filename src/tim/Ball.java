package tdb;

public class Ball{

	int radius = 1;
	int x = 0;
	int y = 0;
	
	
	public Ball( int r ){
		radius = r;
	}
	
	
	public Ball( int r, int x, int y ){
		this( r );
		this.x = x;
		this.y = y;
	}
	
}
	
	