package tdb;

public class Cat{

	String name;
	int age = 0;
	
	
	public Cat( String s ){
		setName( s );
		age = 0;
	}
	
	
	public String toString(){
		return name;
	}
	
}